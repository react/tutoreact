import { Input } from "./components/forms/Input.jsx"
import { Checkbox } from "./components/forms/Checkbox.jsx"
import { useState, useEffect, useMemo, useId, useRef } from "react"


// function App() {

//     const [duration, setDuration] = useState(5)
//     const [secondsLeft, setSecondsLeft] = useState(duration)

//     const handleChange = (v) => {
//         setDuration(v)
//         setSecondsLeft(v)
//     }
//     useEffect(() => {
//         const timer = setInterval(() => {
//             // utiliser callback avec v et nettoyer le setInterval
//             setSecondsLeft(v => {
//                 if (v <= 1) {
//                     clearInterval(timer)
//                     return 0
//                 }

//                 return v -1
//             })
//         }, 1000)
//         return () => {
//             clearInterval(timer)
//         }
//     }, [duration]);
//     return <>
//     <Input 
//         placeholder="Timer"
//         value={duration}
//         onChange={handleChange}
//         >

//     </Input>
//     <p> Décompte: {secondsLeft} </p>
// </> 

// }

// USEMEMO
// function App() {
//     const [firstname, setFirstname] = useState("John")
//     const [password, setPassword] = useState("mpd")
//     const security = useMemo(() => {
//         return passwordSecurity(password)
//     }, [password])

//     return <div className="container my-3 vstack gap-2">

//         <Input
//             label="nom utilisateur"
//             value={firstname}
//             onChange={setFirstname}>
//         </Input>

//         <Input
//             label="mdp"
//             type="password"
//             value={password}
//             onChange={setPassword}>
//         </Input>

//         Sécurité = {security}
//     </div>
// }

// function passwordSecurity (password) {
//     // ma fonction
// }

// USEREF
function App () {
    const prefixRef = useRef(null)
    const [prefix, setPrefix] = useState('')
    prefixRef.current = prefix

    useEffect(() => {
        // console.log(ref.current.offsetHeight);
        const timer = setInterval(() => {
            console.log(prefixRef.current);
        }, 1000)
        return () => {
            clearInterval(timer)
        }
    }, []) // le fait de mettre le prefix en dépendance va permettre de réexcécuter le useEffect lorsque le champ change et non pas qu'à l'initialisation

    return <div>
        <Input label="prefix" value={prefix} onChange={setPrefix}></Input>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit.
        Repellat, corrupti accusamus similique sint et explicabo ad autem debitis nostrum exercitationem, 
        rerum repudiandae nobis esse, aperiam iusto. 
        Dicta tempore ipsum quibusdam?
    </div>
}


export default App
