/**
 * 
 * @param {string} placeholder
 * @param {string} value
 * @param {(s: string) => void} onChange
 */

import { useId } from "react"

export function Input ({placeholder, value, onChange, label, inputRef}) {
    const id = useId()
    return <div>
        <input
            ref={inputRef}
            id={id}
            type="text"
            className="form-control"
            value={value}
            placeholder={placeholder}
            onChange={(e) => onChange(e.target.value)} 
            />
    </div>
}